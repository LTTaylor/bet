https://lttaylor.gitlab.io/bet/

# Brief
The Bede Casino is implementing a new betting site that includes a sports betting slip feature that interacts with a RESTful betting slip service. As part of the team, you have been given the following user stories to complete.

# User Stories
1. As a user I should be able to view all available bets.
1. As a user I should be able to select 1 bet which is then displayed in my bet slip.
1. As a user with a betting slip I should be able to enter a stake.
1. As a user with a betting slip containing at least 1 bet I should be able to place my bets, and see the matched odds with potential returns
1. As a user I should see any errors when they occur.
1. As a user I should be able to see my bet receipt
  - Bet name, Accepted odds, Projected payout, Transaction reference.

# What we’re looking for
- Clean state handling. Placed bets will not be stored server side, the client should handle this state efficiently.
- Clean rendering with templates. For example: handling loading states nicely, limit Jank (http://jankfree.org/).
- Separation of concerns, and well structured code.
- User friendly presentation and easy to use interface.
- Encorporate some design flair into the layout and styling of the betting slip, preferably making use of some CSS pre and post processing.
- Implementation using a popular JavaScript framework, following best practices that are common within the community for that framework.

# Extra credit
- Add some sort of automated tests.
- Add more than one bet to the betslip and submit them all (separate api calls).
- Support switching to decimal formatted odds (see wikipedia).
- The use of ES6 features.
- Making use of Bede frontend tooling: Backbone, Marionette, Gulp, Babel, SCSS etc.

# Requirements
The latest version of Firefox or Chrome (or any browser that supports CORS).

# Notes
Use of existing frameworks and libraries is allowed, even encouraged as long as the resulting code is easy to understand, and you can explain the reasons behind your choice.

# Deliverables
Your implementation should have a step by step installation guide so that we can run and test your code. Submissions should be in the form of a git repository, both hosted or a zip file are acceptable.

# Glossary of terms
1. Slip: ​The shopping basket of bets the user wishes to place.
1. Stake:​The amount of money the user chooses to place on the bet.
1. Returns:​How much would be paid out if the bet won (multiply the odds and the stake, then add the stake).
1. Evens: ​The way a price should display if the odds are 1/1.

# API Docs
The API is reachable at https://bedefetechtest.herokuapp.com/v1/.

_Note that the API does not store any data after it has returned the response._

## GET /markets
Returns JSON array of bet objects.

```json
[
  {
    "bet_id":1,
    "event":"NextWorldCup",
    "name":"England",
    "odds":{
      "numerator":10,
      "denominator":1
    }
  }
]
```

## POST /bets
Accepts JSON bet object with stake.

```json
{
  "bet_id":1,
  "odds":{
    "numerator":10,
    "denominator":1
  },
  "stake":10
}
```

On failure will return 400: Bad Request and a JSON error object

```json
{
  "statusCode":400,
  "error":"BadRequest",
  "message":"Invalidbet_id",
  "validation":{
    "source":"payload",
    "keys":[
      "bet_id"
    ]
  }
}
```

On success will return 201: Created and a JSON receipt object

```json
{
  "bet_id":1,
  "event":"NextWorldCup",
  "name":"England",
  "odds":{
    "numerator":10,
    "denominator":1
  },
  "stake":10,
  "transaction_id":1234
}
```


