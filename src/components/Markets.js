import React, { Component } from 'react';

import Market from './Market';

class Markets extends Component {
    renderMarket(index) {
        return (
            <Market
                event={this.props.markets[index].event}
                key={index}
                market={this.props.markets[index]}
                name={this.props.markets[index].name}
                odds={this.props.markets[index].odds}
                renderOdds={this.props.renderOdds}
                toggleMarket={this.props.toggleMarket}
                marketSelected={this.props.marketSelected}
            />
        );
    }

    render() {
        return (
            <section className="section section--markets">
                <div className="section__container">
                    <div className="section__title">
                        <h2 className="section__title-text">Markets</h2>

                        <button
                            className="button button--odds-switch"
                            onClick={this.props.switchOdds}>
                            Switch to{' '}
                            {this.props.fractions ? 'decimals' : 'fractions'}
                        </button>
                    </div>

                    {Object.keys(this.props.markets).map(index =>
                        this.renderMarket(index),
                    )}
                </div>
            </section>
        );
    }
}

export default Markets;
