import React, { Component } from 'react';
import axios from 'axios';

import '../styles/app.css';

import Header from './Header';
import Markets from './Markets';
import Betslip from './Betslip';
import Receipts from './Receipts';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            error: null,
            fractions: false,
            loading: true,
            markets: {},
            selectedBetIds: [],
            selectedBets: [],
            totalReturns: 0,
            totalStake: 0,
            receipts: [],
        };

        this.fetchMarkets = this.fetchMarkets.bind(this);
        this.fetchSelectedBets = this.fetchSelectedBets.bind(this);
        this.fetchReceipts = this.fetchReceipts.bind(this);
        this.toggleMarket = this.toggleMarket.bind(this);
        this.placeBet = this.placeBet.bind(this);
        this.removeBet = this.removeBet.bind(this);
        this.addStakeAndReturnsToSelectedMarket = this.addStakeAndReturnsToSelectedMarket.bind(
            this,
        );
        this.calculateTotalStake = this.calculateTotalStake.bind(this);
        this.calculateTotalReturns = this.calculateTotalReturns.bind(this);
        this.renderOdds = this.renderOdds.bind(this);
        this.switchOdds = this.switchOdds.bind(this);
        this.marketSelected = this.marketSelected.bind(this);
        this.processBet = this.processBet.bind(this);
        this.processBetError = this.processBetError.bind(this);
    }

    componentDidMount() {
        this.fetchMarkets();
        this.fetchSelectedBets();
        this.fetchReceipts();
    }

    fetchMarkets() {
        const cachedMarkets = localStorage.getItem('markets');

        if (cachedMarkets) {
            this.setState({
                markets: JSON.parse(cachedMarkets),
                loading: false,
                error: null,
            });
        } else {
            axios
                .get(`https://bedefetechtest.herokuapp.com/v1/markets`)
                .then(response => {
                    const markets = response.data;

                    localStorage.setItem('markets', JSON.stringify(markets));

                    this.setState((prevState, props) => {
                        return {
                            markets,
                            loading: false,
                            error: null,
                        };
                    });
                })
                .catch(error => {
                    this.setState((prevState, props) => {
                        return {
                            loading: false,
                            error,
                        };
                    });
                });
        }

        return;
    }

    fetchSelectedBets() {
        const cachedBets = localStorage.getItem('selectedBets');
        const cachedBetIds = localStorage.getItem('selectedBetIds');

        if (cachedBets) {
            this.setState({
                selectedBets: JSON.parse(cachedBets),
            });
        }

        if (cachedBetIds) {
            this.setState({
                selectedBetIds: JSON.parse(cachedBetIds),
            });
        }

        return;
    }

    fetchReceipts() {
        const cachedReceipts = localStorage.getItem('receipts');

        if (cachedReceipts) {
            this.setState({
                receipts: JSON.parse(cachedReceipts),
            });
        }

        return;
    }

    renderLoading() {
        return <div>Loading...</div>;
    }

    renderError() {
        return <div>Something went wrong: {this.state.error.message}</div>;
    }

    renderMarkets() {
        const { error, markets } = this.state;

        if (error) {
            return this.renderError();
        }

        return (
            <Markets
                fractions={this.state.fractions}
                markets={markets}
                renderOdds={this.renderOdds}
                toggleMarket={this.toggleMarket}
                switchOdds={this.switchOdds}
                marketSelected={this.marketSelected}
            />
        );
    }

    toggleMarket(market) {
        if (this.marketSelected(market.bet_id)) {
            this.removeBet(market.bet_id);
        } else {
            this.setState((prevState, props) => {
                if (
                    !this.state.selectedBets.find(
                        bet => bet.bet_id === market.bet_id,
                    )
                ) {
                    localStorage.setItem(
                        'selectedBets',
                        JSON.stringify([...prevState.selectedBets, market]),
                    );

                    localStorage.setItem(
                        'selectedBetIds',
                        JSON.stringify([
                            ...prevState.selectedBetIds,
                            market.bet_id,
                        ]),
                    );

                    return {
                        selectedBets: [...prevState.selectedBets, market],
                        selectedBetIds: [
                            ...prevState.selectedBetIds,
                            market.bet_id,
                        ],
                    };
                } else {
                    return prevState;
                }
            });
        }
    }

    placeBet() {
        this.state.selectedBets.forEach(bet => {
            const payload = {
                bet_id: bet.bet_id,
                odds: bet.odds,
                stake: bet.stake,
            };

            axios
                .post(`https://bedefetechtest.herokuapp.com/v1/bets`, payload)
                .then(response => {
                    this.processBet(response.data);
                })
                .catch(error => {
                    this.processBetError(error.response.data, payload.bet_id);
                    console.error(error.response.data.message);
                });
        });
    }

    processBet(transaction) {
        let bet = transaction;

        bet.returns =
            bet.stake * (bet.odds.numerator / bet.odds.denominator) + bet.stake;

        this.removeBet(bet.bet_id);

        this.setState((prevState, props) => {
            localStorage.setItem(
                'receipts',
                JSON.stringify([...prevState.receipts, bet]),
            );

            return {
                receipts: [...prevState.receipts, bet],
            };
        });
    }

    processBetError(error, bet_id) {
        this.setState(() => {
            let selectedBets = this.state.selectedBets,
                selectedBetIndex = selectedBets.findIndex(
                    bet => bet.bet_id === bet_id,
                ),
                selectedBet = selectedBets.find(bet => bet.bet_id === bet_id);

            selectedBet.error = error;

            selectedBets[selectedBetIndex] = selectedBet;

            return {
                selectedBets,
            };
        });
    }

    removeBet(bet_id) {
        const index = this.state.selectedBets.findIndex(
            bet => bet.bet_id === bet_id,
        );

        this.setState(
            (prevState, props) => {
                localStorage.setItem(
                    'selectedBets',
                    JSON.stringify([
                        ...prevState.selectedBets.slice(0, index),
                        ...prevState.selectedBets.slice(index + 1),
                    ]),
                );

                localStorage.setItem(
                    'selectedBetIds',
                    JSON.stringify([
                        ...prevState.selectedBetIds.slice(0, index),
                        ...prevState.selectedBetIds.slice(index + 1),
                    ]),
                );

                return {
                    selectedBets: [
                        ...prevState.selectedBets.slice(0, index),
                        ...prevState.selectedBets.slice(index + 1),
                    ],
                    selectedBetIds: [
                        ...prevState.selectedBetIds.slice(0, index),
                        ...prevState.selectedBetIds.slice(index + 1),
                    ],
                };
            },
            () => {
                this.calculateTotalStake();
                this.calculateTotalReturns();
            },
        );
    }

    addStakeAndReturnsToSelectedMarket(bet_id, stake, returns) {
        this.setState(
            (prevState, props) => {
                if (
                    this.state.selectedBets.find(bet => bet.bet_id === bet_id)
                ) {
                    let selectedBets = this.state.selectedBets,
                        selectedBetIndex = selectedBets.findIndex(
                            bet => bet.bet_id === bet_id,
                        ),
                        selectedBet = selectedBets.find(
                            bet => bet.bet_id === bet_id,
                        );

                    selectedBet.stake = stake;
                    selectedBet.returns = returns;

                    selectedBets[selectedBetIndex] = selectedBet;

                    return {
                        selectedBets,
                    };
                } else {
                    return prevState;
                }
            },
            () => {
                this.calculateTotalStake();
                this.calculateTotalReturns();
            },
        );
    }

    calculateTotalStake() {
        let totalStake = 0;

        this.state.selectedBets.forEach(bet => {
            if (
                bet.stake !== '' &&
                !isNaN(bet.stake) &&
                typeof bet.stake !== 'undefined'
            ) {
                totalStake += parseInt(bet.stake, 10);
            }
        });

        this.setState({ totalStake: totalStake });
    }

    calculateTotalReturns() {
        let totalReturns = 0;

        this.state.selectedBets.forEach(bet => {
            if (
                bet.returns !== '' &&
                !isNaN(bet.returns) &&
                typeof bet.returns !== 'undefined'
            ) {
                totalReturns += parseFloat(bet.returns);
            }
        });

        this.setState({ totalReturns: totalReturns });
    }

    renderOdds(odds) {
        if (odds.numerator === odds.denominator) {
            return <span className="odds odds--evens">@Evens</span>;
        } else if (this.state.fractions === true) {
            return (
                <span className="odds odds--fraction">
                    @{odds.numerator}/{odds.denominator}
                </span>
            );
        }

        return (
            <span className="odds odds--decimal">
                @{odds.numerator / odds.denominator + 1}
            </span>
        );
    }

    switchOdds() {
        this.setState({ fractions: !this.state.fractions });
    }

    marketSelected(bet_id) {
        if (this.state.selectedBetIds.includes(bet_id)) {
            return true;
        }

        return false;
    }

    render() {
        const { loading } = this.state;

        return (
            <div className="App">
                <Header />

                <main className="main">
                    {loading ? this.renderLoading() : this.renderMarkets()}

                    <section className="section section--aside">
                        <Betslip
                            selectedBets={this.state.selectedBets}
                            placeBet={this.placeBet}
                            removeBet={this.removeBet}
                            addStakeAndReturnsToSelectedMarket={
                                this.addStakeAndReturnsToSelectedMarket
                            }
                            totalStake={this.state.totalStake}
                            totalReturns={this.state.totalReturns}
                            renderOdds={this.renderOdds}
                        />

                        <Receipts
                            receipts={this.state.receipts}
                            renderOdds={this.renderOdds}
                        />
                    </section>
                </main>

                <aside className="aside" />
            </div>
        );
    }
}

export default App;
