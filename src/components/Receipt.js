import React, { Component } from 'react';

class Receipt extends Component {
    render() {
        return (
            <div className="market market--receipt">
                <div className="market__event">{this.props.receipt.event}</div>

                <div className="market__name">
                    <strong>{this.props.receipt.name}</strong>
                </div>

                <div className="market__odds">
                    Accepted Odds:{' '}
                    {this.props.renderOdds(this.props.receipt.odds)}
                </div>

                <div className="market__stake">
                    <strong>Stake:</strong>{' '}
                    {this.props.receipt.stake.toLocaleString('en-GB', {
                        style: 'currency',
                        currency: 'GBP',
                    })}
                </div>

                <div className="market__returns">
                    Projected Payout:{' '}
                    <strong>
                        {this.props.receipt.returns.toLocaleString('en-GB', {
                            style: 'currency',
                            currency: 'GBP',
                        })}
                    </strong>
                </div>

                <div className="market__ref">
                    <em>
                        Transaction Reference:{' #'}
                        {this.props.receipt.transaction_id}
                    </em>
                </div>
            </div>
        );
    }
}

export default Receipt;
