import React, { Component } from 'react';

import Receipt from './Receipt';

class Receipts extends Component {
    renderReceipt(index) {
        return (
            <Receipt
                key={index}
                receipt={this.props.receipts[index]}
                renderOdds={this.props.renderOdds}
            />
        );
    }

    render() {
        return (
            <section className="section section--receipts">
                <div className="section__container">
                    <div className="section__title">
                        <h2 className="section__title-text">Receipts</h2>
                    </div>

                    {Object.keys(this.props.receipts).map(index =>
                        this.renderReceipt(index),
                    )}
                </div>
            </section>
        );
    }
}

export default Receipts;
