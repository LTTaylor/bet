import React, { Component } from 'react';

class Market extends Component {
    render() {
        return (
            <div
                className={
                    this.props.marketSelected(this.props.market.bet_id)
                        ? 'market market--selected'
                        : 'market'
                }
                onClick={() => this.props.toggleMarket(this.props.market)}>
                <div className="market__event">{this.props.event}</div>

                <div className="market__name">
                    <strong>{this.props.name}</strong>
                </div>

                <div className="market__odds">
                    {this.props.renderOdds(this.props.odds)}
                </div>
            </div>
        );
    }
}

export default Market;
