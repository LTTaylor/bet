import React, { Component } from 'react';

class Bet extends Component {
    constructor(props) {
        super(props);

        this.state = {
            stake: 0,
            returns: 0,
        };

        this.handleChange = this.handleChange.bind(this);
        this.renderError = this.renderError.bind(this);
    }

    handleChange(event) {
        if (
            event.target.value === '' ||
            event.target.value === 0 ||
            isNaN(event.target.value) === true
        ) {
            this.setState({
                stake: 0,
                returns: 0,
            });

            this.props.addStakeAndReturnsToSelectedMarket(
                this.props.betId,
                0,
                0,
            );
        } else {
            this.setState({ stake: event.target.value }, () => {
                this.setState(
                    (prevState, props) => {
                        const stake = parseInt(this.state.stake, 10);
                        const odds =
                            props.odds.numerator / props.odds.denominator;

                        return {
                            returns: stake * odds + stake,
                        };
                    },
                    () => {
                        this.props.addStakeAndReturnsToSelectedMarket(
                            this.props.betId,
                            this.state.stake,
                            this.state.returns,
                        );
                    },
                );
            });
        }
    }

    renderError() {
        if (this.props.error) {
            return (
                <div className="market__error">
                    {this.props.error.error}: {this.props.error.message}
                </div>
            );
        }
    }

    render() {
        return (
            <div className="market market--bet">
                {this.renderError()}

                <div className="market__event">{this.props.event}</div>

                <div className="market__name">
                    <strong>{this.props.name}</strong>
                </div>

                <div className="market__odds">
                    {this.props.renderOdds(this.props.odds)}
                </div>

                <div className="market__stake">
                    <label className="market__stake-label">Stake:</label>
                    <input
                        className="market__stake-input"
                        type="number"
                        min="0"
                        step="1"
                        placeholder="0"
                        onChange={this.handleChange}
                        value={this.state.stake}
                    />
                </div>

                <div className="market__returns">
                    Returns:{' '}
                    <strong>
                        {this.state.returns.toLocaleString('en-GB', {
                            style: 'currency',
                            currency: 'GBP',
                        })}
                    </strong>
                </div>

                <button
                    className="button button--secondary"
                    onClick={() => this.props.removeBet(this.props.betId)}>
                    Remove
                </button>
            </div>
        );
    }
}

export default Bet;
