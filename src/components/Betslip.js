import React, { Component } from 'react';

import Bet from './Bet';

class Betslip extends Component {
    renderBet(bet) {
        return (
            <Bet
                addStakeAndReturnsToSelectedMarket={
                    this.props.addStakeAndReturnsToSelectedMarket
                }
                betId={bet.bet_id}
                event={bet.event}
                key={bet.bet_id}
                name={bet.name}
                odds={bet.odds}
                removeBet={this.props.removeBet}
                renderOdds={this.props.renderOdds}
                error={bet.error}
            />
        );
    }

    render() {
        return (
            <section className="section section--betslip">
                <div className="section__container">
                    <div className="section__title">
                        <h2 className="section__title-text">Betslip</h2>
                    </div>

                    {this.props.selectedBets.map(bet => this.renderBet(bet))}

                    <div className="totals">
                        <div className="totals__total-stake">
                            Total stake:{' '}
                            {this.props.totalStake.toLocaleString('en-GB', {
                                style: 'currency',
                                currency: 'GBP',
                            })}
                        </div>
                        <div className="totals__potential-returns">
                            Total returns:{' '}
                            {this.props.totalReturns.toLocaleString('en-GB', {
                                style: 'currency',
                                currency: 'GBP',
                            })}
                        </div>
                    </div>

                    <button
                        className="button button--primary"
                        onClick={this.props.placeBet}>
                        Place bets
                    </button>
                </div>
            </section>
        );
    }
}

export default Betslip;
