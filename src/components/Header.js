import React, { Component } from 'react';

export class Header extends Component {
    render() {
        return (
            <header className="header">
                <div className="header__container">
                    <h1 className="header__title">
                        <span className="header__highlight">LTT</span> Bet!
                    </h1>
                </div>
            </header>
        );
    }
}

export default Header;
